/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.edgegallery.springplugin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.edgegallery.packageupload.Connection;
import com.edgegallery.packageupload.UploadPackageEntity;
import com.edgegallery.packageupload.Utils;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service("PluginService")
public class PluginService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginService.class);

    /**
     * Map<String, String> body = new HashMap<>();
     * body.put("meaoUrl", meaoUrl);
     * body.put("username", username);
     * body.put("password", password);
     *
     * @param meaoInfo JsonObject
     * @return String
     */
    public String getSession(JSONObject meaoInfo) {
        LOGGER.info("Get session from meao.");
        String meaoUrl = meaoInfo.getString("meaoUrl");
        String userName = meaoInfo.getString("username");
        String password = meaoInfo.getString("password");
        return Utils.getSessionString(meaoUrl, userName, password);
    }

    /**
     * uploadFile.
     * Map<String, Object> body = new HashMap<>();
     * body.put("header", header);
     * body.put("uploadUrl", uploadUrl);
     * body.put("upPackage", upPackage);
     * body.put("req", req);
     * body.put("buffer", buffer);
     *
     * @return result
     */
    public String uploadFile(String header, String uploadUrl, UploadPackageEntity upPackage, String req,
        MultipartFile file) {
/*        LOGGER.info("uploadFile: {}", uploadInfo.get("header"));
        String header = (String) uploadInfo.get("header");
        LOGGER.info("header");
        String uploadUrl = (String) uploadInfo.get("uploadUrl");
        LOGGER.info("uploadUrl");
        String upPackage = (String) uploadInfo.get("upPackage");
        LOGGER.info("upPackage");
        String req = (String) uploadInfo.get("req");
        LOGGER.info("req: {}", req);
        byte[] postData = uploadInfo.get("buffer").getBytes();
        LOGGER.info("postData");*/
        // LOGGER.info("postData: {}", postData);

        LOGGER.info("header: {}", header);
        LOGGER.info("uploadUrl: {}", uploadUrl);
        LOGGER.info("upPackage: {}", JSONObject.toJSON(upPackage));
        LOGGER.info("req: {}", req);

        byte[] buffer = new byte[0];
        try {
            buffer = file.getBytes();
        } catch (IOException e) {
            LOGGER.error("File to byte failed, message: {}", e.getMessage());
        }

        return Connection.postFiles(JSON.parseObject(header), uploadUrl, upPackage, JSON.parseObject(req), buffer)
            .toString();
    }
}
