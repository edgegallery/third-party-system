/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.edgegallery.springplugin;

import com.alibaba.fastjson.JSONObject;
import com.edgegallery.packageupload.UploadPackageEntity;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/mec/third-system/v1/meao/huawei")
public class PluginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginController.class);

    @GetMapping
    public ResponseEntity<String> greetMVC() {
        return ResponseEntity.ok().body("Huawei meao plugin.");
    }

    @PostMapping(value = "/session", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "get session info", response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 500, message = "resource grant error", response = String.class)})
    public ResponseEntity<String> getSession(@RequestBody JSONObject meaoInfo) {
        return ResponseEntity.ok().body(new PluginService().getSession(meaoInfo));
    }

    @PostMapping(value = "/action/upload", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "upload file to meao", response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 500, message = "resource grant error", response = String.class)})
    public ResponseEntity<String> uploadFile3(@RequestPart("header") String header,
        @RequestPart("uploadUrl") String uploadUrl, @RequestPart("upPackage") UploadPackageEntity upPackage,
        @RequestPart("req") String req, @RequestPart("file") MultipartFile file) {
        LOGGER.info("uploadFile controller enter.");
        return ResponseEntity.ok().body(new PluginService().uploadFile(header, uploadUrl, upPackage, req, file));
    }

}
