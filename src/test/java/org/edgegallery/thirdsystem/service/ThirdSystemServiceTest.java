/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.edgegallery.thirdsystem.bean.vo.ThirdSystemVo;
import org.edgegallery.thirdsystem.repository.ThirdSystem;
import org.edgegallery.thirdsystem.repository.mapper.ThirdSystemMapper;
import org.edgegallery.thirdsystem.utils.exception.AppException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class ThirdSystemServiceTest {
    @Mock
    private ThirdSystemMapper thirdSystemMapper;

    @InjectMocks
    private ThirdSystemService thirdSystemService;

    @Test
    public void testCreateThirdSystemSuccess() throws IOException {
        ThirdSystem thirdSystem = new ThirdSystem();
        thirdSystem.setSystemName("testName");
        thirdSystem.setConfigContent("ZG5zc2VydmVyOgogIGNvbW1hbmQ6IFtdCiAgc3RvcmFnZVNpemU6IDFHaQplbGFzdGljc2VhcmNoOgogIHN0b3JhZ2VTaXplOiAxR2kKZ2xvYmFsOgogIHBlcnNpc3RlbmNlOgogICAgZW5hYmxlZDogZmFsc2UKICAgIHN0b3JhZ2VDbGFzc05hbWU6IG5mcy1jbGllbnQKaW1hZ2VzOgogIGRuczoKICAgIHB1bGxQb2xpY3k6IElmTm90UHJlc2VudAogICAgcmVwb3NpdG9yeTogc3dyLmFwLXNvdXRoZWFzdC0xLm15aHVhd2VpY2xvdWQuY29tL2VkZ2VnYWxsZXJ5L21lcC1kbnMtc2VydmVyCiAgICB0YWc6IGxhdGVzdAogIGVsYXN0aWNzZWFyY2g6CiAgICBwdWxsUG9saWN5OiBJZk5vdFByZXNlbnQKICAgIHJlcG9zaXRvcnk6IHN3ci5hcC1zb3V0aGVhc3QtMS5teWh1YXdlaWNsb3VkLmNvbS9lZy1jb21tb24vZWxhc3RpY3NlYXJjaAogICAgdGFnOiA3LjkuMAogIGtvbmc6CiAgICBwdWxsUG9saWN5OiBJZk5vdFByZXNlbnQKICAgIHJlcG9zaXRvcnk6IHN3ci5hcC1zb3V0aGVhc3QtMS5teWh1YXdlaWNsb3VkLmNvbS9lZy1jb21tb24va29uZwogICAgdGFnOiAyLjAuNC11YnVudHUKICBtZXA6CiAgICBwdWxsUG9saWN5OiBJZk5vdFByZXNlbnQKICAgIHJlcG9zaXRvcnk6IHN3ci5hcC1zb3V0aGVhc3QtMS5teWh1YXdlaWNsb3VkLmNvbS9lZGdlZ2FsbGVyeS9tZXAKICAgIHRhZzogbGF0ZXN0CiAgbWVwYXV0aDoKICAgIHB1bGxQb2xpY3k6IElmTm90UHJlc2VudAogICAgcmVwb3NpdG9yeTogc3dyLmFwLXNvdXRoZWFzdC0xLm15aHVhd2VpY2xvdWQuY29tL2VkZ2VnYWxsZXJ5L21lcGF1dGgKICAgIHRhZzogbGF0ZXN0CiAgbnRwOgogICAgcHVsbFBvbGljeTogSWZOb3RQcmVzZW50CiAgICByZXBvc2l0b3J5OiBzd3IuYXAtc291dGhlYXN0LTEubXlodWF3ZWljbG91ZC5jb20vZWctY29tbW9uL2N0dXJyYS9udHAKICAgIHRhZzogbGF0ZXN0CiAgcG9zdGdyZXM6CiAgICBwdWxsUG9saWN5OiBJZk5vdFByZXNlbnQKICAgIHJlcG9zaXRvcnk6IHN3ci5hcC1zb3V0aGVhc3QtMS5teWh1YXdlaWNsb3VkLmNvbS9lZy1jb21tb24vcG9zdGdyZXMKICAgIHRhZzogMTIuMwpsaXZlbmVzc1Byb2JlOgogIGZhaWx1cmVUaHJlc2hvbGQ6IDMKICBpbml0aWFsRGVsYXlTZWNvbmRzOiAzMAogIHBhdGg6IC9oZWFsdGgKICBwZXJpb2RTZWNvbmRzOiAxMAogIHNjaGVtZTogSFRUUFMKICBzY2hlbWVfaHR0cDogSFRUUAogIHN1Y2Nlc3NUaHJlc2hvbGQ6IDEKICB0aW1lb3V0U2Vjb25kczogNQptZXA6CiAgc3RvcmFnZVNpemU6IDFHaQptZXBBdXRoTGl2ZW5lc3NQcm9iZToKICBmYWlsdXJlVGhyZXNob2xkOiAzMAogIGluaXRpYWxEZWxheVNlY29uZHM6IDMwCiAgcGF0aDogL2hlYWx0aAogIHBlcmlvZFNlY29uZHM6IDEwCiAgc2NoZW1lOiBIVFRQUwogIHNjaGVtZV9odHRwOiBIVFRQCiAgc3VjY2Vzc1RocmVzaG9sZDogMQogIHRpbWVvdXRTZWNvbmRzOiA1Cm1lcEF1dGhSZWFkaW5lc3NQcm9iZToKICBmYWlsdXJlVGhyZXNob2xkOiAzMAogIGluaXRpYWxEZWxheVNlY29uZHM6IDE1CiAgcGF0aDogL2hlYWx0aAogIHBlcmlvZFNlY29uZHM6IDEwCiAgc2NoZW1lOiBIVFRQUwogIHNjaGVtZV9odHRwOiBIVFRQCiAgc3VjY2Vzc1RocmVzaG9sZDogMQogIHRpbWVvdXRTZWNvbmRzOiA1Cm1lcENvbmZpZzoKICBkYXRhUGxhbmVUeXBlOiBub25lCiAgZG5zQWdlbnRUeXBlOiBhbGwKbmV0d29ya0lzb2xhdGlvbjoKICBpcGFtVHlwZTogaG9zdC1sb2NhbAogIHBoeUludGVyZmFjZToKICAgIG1tNTogZW5zMwogICAgbXAxOiBlbnMzCm50cENvbmZpZzoKICBzZXJ2ZXJzOiBjbi5wb29sLm50cC5vcmcsIGFzaWEucG9vbC5udHAub3JnLCBwb29sLm50cC5vcmcKcG9zdGdyZXM6CiAgZGJuYW1lOiBrb25nZGIKICBwYXNzd29yZDogbnVsbAogIHN0b3JhZ2VTaXplOiAxR2kKICB1c2VybmFtZToga29uZwogIGtvbmdQYXNzOiAiXCd0ZTlGbXYlcWFxXCciCnJlYWRpbmVzc1Byb2JlOgogIGZhaWx1cmVUaHJlc2hvbGQ6IDMKICBpbml0aWFsRGVsYXlTZWNvbmRzOiAxNQogIHBhdGg6IC9oZWFsdGgKICBwZXJpb2RTZWNvbmRzOiAxMAogIHNjaGVtZTogSFRUUFMKICBzY2hlbWVfaHR0cDogSFRUUAogIHN1Y2Nlc3NUaHJlc2hvbGQ6IDEKICB0aW1lb3V0U2Vjb25kczogNQpzc2w6CiAgZW5hYmxlZDogZmFsc2UKICBzZWNyZXROYW1lOiBudWxsCg==");
        Mockito.when(thirdSystemMapper.insertSelective(thirdSystem)).thenReturn(1);
        ResponseEntity<String> ret = thirdSystemService.createThirdSystem(thirdSystem);
        Assert.assertNotNull(ret.getBody());
    }

    @Test(expected = AppException.class)
    public void testGetThirdSystemByIdFail() {
        Mockito.when(thirdSystemMapper.selectByPrimaryKey(Mockito.anyString())).thenReturn(null);
        thirdSystemService.getThirdSystemById("meao");
    }

    @Test(expected = AppException.class)
    public void testGetThirdSystemByTypeFail() {
        Mockito.when(thirdSystemMapper.selectBySystemType(Mockito.anyString())).thenReturn(null);
        thirdSystemService.getThirdSystemByType("meao");
    }

    @Test
    public void testSelectByNameLikeSuccess() {
        ThirdSystem thirdSystem = new ThirdSystem();
        thirdSystem.setSystemName("testName");
        List<ThirdSystem> list = new ArrayList<>();
        list.add(thirdSystem);
        Mockito.when(thirdSystemMapper.selectByNameLike(Mockito.anyString(), Mockito.anyString())).thenReturn(list);
        ResponseEntity<List<ThirdSystemVo>> ret = thirdSystemService.selectByNameLike("testName", "meao");
        Assert.assertNotNull(ret.getBody());
    }

    @Test(expected = AppException.class)
    public void testSelectByNameLikeFail() {
        Mockito.when(thirdSystemMapper.selectByNameLike(Mockito.anyString(), Mockito.anyString())).thenReturn(null);
        thirdSystemService.selectByNameLike("1234", "meao");
    }

    @Test(expected = AppException.class)
    public void testUpdateThirdSystemNotFound() {
        Mockito.when(thirdSystemMapper.selectByPrimaryKey(Mockito.anyString())).thenReturn(null);
        thirdSystemService.updateThirdSystem("1234", new ThirdSystem());
    }

    @Test(expected = AppException.class)
    public void testUpdateThirdSystemFail() {
        Mockito.when(thirdSystemMapper.selectByPrimaryKey(Mockito.anyString())).thenReturn(new ThirdSystem());
        Mockito.when(thirdSystemMapper.updateByPrimaryKeySelective(Mockito.any())).thenReturn(-1);
        thirdSystemService.updateThirdSystem("1234", new ThirdSystem());
    }

    @Test
    public void testDeleteThirdSystemSuccess() {
        Mockito.when(thirdSystemMapper.deleteByPrimaryKey(Mockito.anyString())).thenReturn(1);
        ResponseEntity<String> ret = thirdSystemService.deleteThirdSystem("1234");
        Assert.assertEquals("delete third system success.", ret.getBody());
    }

    @Test(expected = AppException.class)
    public void testDeleteThirdSystemFail() {
        Mockito.when(thirdSystemMapper.deleteByPrimaryKey(Mockito.anyString())).thenReturn(-1);
        thirdSystemService.deleteThirdSystem("1234");
    }
}