/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.controller.advice;

import javax.servlet.http.HttpServletRequest;
import org.edgegallery.thirdsystem.utils.exception.AppException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.access.AccessDeniedException;

@RunWith(MockitoJUnitRunner.class)
public class GlobalExceptionConvertTest {
    @Mock
    HttpServletRequest request;

    @Mock
    RuntimeException runtimeException;

    @Mock
    BadSqlGrammarException badSqlGrammarException;

    @Mock
    AccessDeniedException accessDeniedException;

    @Mock
    IllegalArgumentException illegalArgumentException;

    @Mock
    Exception exception;

    @InjectMocks
    private GlobalExceptionConvert convert;

    @Test()
    public void testAppException() {
        AppException exception = new AppException("appException", 1234);
        RestReturn ret = convert.appException(request, exception);
        Assert.assertNotNull(ret);
    }

    @Test()
    public void testRuntimeException() {
        RestReturn ret = convert.runtimeException(request, runtimeException);
        Assert.assertNotNull(ret);
    }

    @Test()
    public void testBadSqlGrammarException() {
        RestReturn ret = convert.runtimeException(request, badSqlGrammarException);
        Assert.assertNotNull(ret);
    }

    @Test()
    public void testAccessDeniedException() {
        RestReturn ret = convert.accessDeniedException(request, accessDeniedException);
        Assert.assertNotNull(ret);
    }

    @Test()
    public void testIllegalArgumentException() {
        RestReturn ret = convert.illegalArgumentException(request, illegalArgumentException);
        Assert.assertNotNull(ret);
    }

    @Test()
    public void testDefaultException() {
        RestReturn ret = convert.defaultException(request, exception);
        Assert.assertNotNull(ret);
    }
}
