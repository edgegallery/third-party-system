/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;


import com.google.gson.Gson;
import org.edgegallery.thirdsystem.ApplicationTest;
import org.edgegallery.thirdsystem.repository.ThirdSystem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTest.class)
@AutoConfigureMockMvc
public class ThirdSystemTest {
    @Autowired
    private MockMvc mvc;

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testCreateThirdSystem() throws Exception {
        ThirdSystem thirdSystem = new ThirdSystem();
        thirdSystem.setSystemName("name");
        thirdSystem.setSystemType("type");
        thirdSystem.setPassword("1234");
        thirdSystem.setUrl("http://127.0.0.1:1234");
        thirdSystem.setProduct("oss");
        thirdSystem.setRegion("china");
        thirdSystem.setStatus("active");
        thirdSystem.setVendor("eg");

        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.post("/mec/third-system/v1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new Gson().toJson(thirdSystem)).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testGetThirdSystem() throws Exception {
        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.get("/mec/third-system/v1/f95d34cc-e114-4f15-8ad2-2f4819542b0a")
                .contentType(MediaType.APPLICATION_JSON_VALUE).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testGetThirdSystemList() throws Exception {
        String[] types = {"meao"};
        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.post("/mec/third-system/v1/count").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new Gson().toJson(types)).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testGetThirdSystemByType() throws Exception {
        MvcResult mvcResultQueryAll = mvc.perform(MockMvcRequestBuilders.get("/mec/third-system/v1/systemType/meao")
            .contentType(MediaType.APPLICATION_JSON_VALUE).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testGetThirdSystemByLikeNmae() throws Exception {
        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.get("/mec/third-system/v1/nameLike/MEAO/systemType/meao")
                .contentType(MediaType.APPLICATION_JSON_VALUE).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testUpdateThirdSystem() throws Exception {
        ThirdSystem thirdSystem = new ThirdSystem();
        thirdSystem.setSystemName("newName");

        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.put("/mec/third-system/v1/f95d34cc-e114-4f15-8ad2-2f4819542b0a")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(new Gson().toJson(thirdSystem)).with(csrf())
                .accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }

    @WithMockUser(roles = "APPSTORE_ADMIN")
    @Test
    public void testDeleteThirdSystem() throws Exception {
        MvcResult mvcResultQueryAll = mvc.perform(
            MockMvcRequestBuilders.delete("/mec/third-system/v1/f95d34cc-e114-4f15-8ad2-2f481954dele")
                .contentType(MediaType.APPLICATION_JSON_VALUE).with(csrf()).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int resultQueryAll = mvcResultQueryAll.getResponse().getStatus();
        assertEquals(200, resultQueryAll);
    }
}
