/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


CREATE TABLE IF NOT EXISTS THIRD_SYSTEM_TABLE (
    ID VARCHAR(100) NOT NULL,
    SYSTEM_NAME VARCHAR(100) DEFAULT NULL,
    SYSTEM_TYPE VARCHAR(100) DEFAULT NULL,
    USER_ID VARCHAR(100) DEFAULT NULL,
    VERSION VARCHAR(100) DEFAULT NULL,
    URL VARCHAR(100) DEFAULT NULL,
    IP VARCHAR(100) DEFAULT NULL,
    PORT VARCHAR(100) DEFAULT NULL,
    REGION VARCHAR(100) DEFAULT NULL,
    USERNAME VARCHAR(100) DEFAULT NULL,
    PASSWORD VARCHAR(100) DEFAULT NULL,
    PRODUCT VARCHAR(100) DEFAULT NULL,
    VENDOR VARCHAR(100) DEFAULT NULL,
    TOKEN_TYPE VARCHAR(100) DEFAULT NULL,
    ICON TEXT DEFAULT NULL,
    CONFIG_CONTENT TEXT DEFAULT NULL,
    STATUS VARCHAR(100) DEFAULT NULL,
    CONSTRAINT THIRD_SYSTEM_TABLE_PKEY PRIMARY KEY (ID)
    );

INSERT INTO third_system_table(
    id, system_name, system_type, version, url, ip, port, region, username, password, product, vendor, token_type, icon, status, user_id, config_content)
VALUES ('f95d34cc-e114-4f15-8ad2-2f4819542b0a', 'huaweiMEAO', 'meao', 'V1', 'https://19.4.2.18:31943', '19.4.2.18',
        '31943', 'beijing', 'testuser', 'vU3gk8g9nApcwxwKZc64nQ==', 'OSS', 'huawei', 'token', '', 'ACTIVE', 'testuserid', '');

INSERT INTO third_system_table(
    id, system_name, system_type, version, url, ip, port, region, username, password, product, vendor, token_type, icon, status, user_id, config_content)
VALUES ('f95d34cc-e114-4f15-8ad2-2f481954dele', 'huaweiMEAO', 'meao', 'V1', 'https://19.4.2.18:31943', '19.4.2.18',
        '31943', 'beijing', 'testuser', 'vU3gk8g9nApcwxwKZc64nQ==', 'OSS', 'huawei', 'token', '', 'ACTIVE', 'testuserid', '');