/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.bean.vo;

import lombok.Getter;
import lombok.Setter;
import org.edgegallery.thirdsystem.repository.ThirdSystem;

@Getter
@Setter
public class ThirdSystemVo {
    private String id;

    private String systemName;

    private String systemType;

    private String userId;

    private String version;

    private String url;

    private String ip;

    private int port;

    private String region;

    private String username;

    private String product;

    private String vendor;

    private String tokenType;

    private String status;

    private String icon;

    private String configContent;

    /**
     * transfer ThirdSystem to ThirdSystemVo.
     *
     * @param thirdSystem ThirdSystem
     * @return ThirdSystemVo
     */
    public static ThirdSystemVo of(ThirdSystem thirdSystem) {
        ThirdSystemVo vo = new ThirdSystemVo();
        vo.id = thirdSystem.getId();
        vo.systemName = thirdSystem.getSystemName();
        vo.systemType = thirdSystem.getSystemType();
        vo.userId = thirdSystem.getUserId();
        vo.version = thirdSystem.getVersion();
        vo.url = thirdSystem.getUrl();
        vo.ip = thirdSystem.getIp();
        vo.port = thirdSystem.getPort();
        vo.region = thirdSystem.getRegion();
        vo.username = thirdSystem.getUsername();
        vo.product = thirdSystem.getProduct();
        vo.vendor = thirdSystem.getVendor();
        vo.tokenType = thirdSystem.getTokenType();
        vo.status = thirdSystem.getStatus();
        vo.icon = thirdSystem.getIcon();
        vo.configContent = thirdSystem.getConfigContent();
        return vo;
    }
}
