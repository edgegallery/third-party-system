/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.constant;

public final class Constant {

    public static final String ACCESS_TOKEN = "access_token";

    public static final String USER_ID = "userId";

    public static final String USER_NAME = "userName";

    public static final String INVALID_ACCESS_TOKEN = "Invalid access token";

    private Constant() {
    }
}
