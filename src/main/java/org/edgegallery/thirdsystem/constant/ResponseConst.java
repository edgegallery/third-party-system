/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.constant;

public class ResponseConst {
    /**
     * the success code.
     */
    public static final int RET_SUCCESS = 0;

    /**
     * the fail code.
     */
    public static final int RET_FAIL = 1;

    /**
     * the pok code.
     */
    public static final int RET_PART_SUCCESS = 5000;

    /**
     * create third system fail.
     */
    public static final int RET_CREATE_THIRD_SYSTEM_FAILED = 15101;

    /**
     * get third system fail.
     */
    public static final int RET_QUERY_THIRD_SYSTEM_FAILED = 15102;

    /**
     * third system not exist.
     */
    public static final int RET_THIRD_SYSTEM_NOT_FOUND = 15103;

    /**
     * update third system fail.
     */
    public static final int RET_UPDATE_THIRD_SYSTEM_FAILED = 15104;

    /**
     * delete third system fail.
     */
    public static final int RET_DELETE_THIRD_SYSTEM_FAILED = 15105;

    private ResponseConst() {
        throw new IllegalStateException("Utility class");
    }

}
