/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.edgegallery.thirdsystem.controller;

import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.apache.servicecomb.provider.rest.common.RestSchema;
import org.edgegallery.thirdsystem.bean.vo.ThirdSystemVo;
import org.edgegallery.thirdsystem.repository.ThirdSystem;
import org.edgegallery.thirdsystem.service.ThirdSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RestSchema(schemaId = "thirdSystem")
@RequestMapping("/mec/third-system/v1")
@Api(tags = {"ThirdSystem Controller"})
@Validated
public class ThirdSystemController {
    @Autowired
    ThirdSystemService thirdSystemService;

    /**
     * create a thirdSystem.
     */
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "create a thirdSystem", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<String> createThirdSystem(@RequestBody ThirdSystem thirdSystem) throws IOException {
        return thirdSystemService.createThirdSystem(thirdSystem);
    }

    /**
     * query a thirdSystem of all info.
     */
    @GetMapping(value = "/{thirdSystemId}", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "get a thirdSystem", response = ThirdSystem.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<ThirdSystem> getThirdSystem(
        @ApiParam(value = "thirdSystemId") @PathVariable("thirdSystemId") String thirdSystemId) {
        return thirdSystemService.getThirdSystemById(thirdSystemId);
    }

    /**
     * query a thirdSystem for web display.
     */
    @GetMapping(value = "/{thirdSystemId}/action/display", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "get a thirdSystem", response = ThirdSystem.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<ThirdSystemVo> getWebThirdSystem(
        @ApiParam(value = "thirdSystemId") @PathVariable("thirdSystemId") String thirdSystemId) {
        return thirdSystemService.getWebThirdSystemById(thirdSystemId);
    }

    /**
     * count all thirdSystems.
     */
    @PostMapping(value = "/count", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "list thirdSystems", response = ThirdSystem.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<JSONArray> getThirdSystemList(
        @ApiParam(value = "types", required = true) @RequestBody String[] types) {
        return thirdSystemService.countThirdSystem(types);
    }

    /**
     * query thirdSystem by type.
     */
    @GetMapping(value = "/systemType/{type}", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "query thirdSystem by type", response = ThirdSystem.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<List<ThirdSystemVo>> getThirdSystemByType(
        @ApiParam(value = "type") @PathVariable("type") String type) {
        return thirdSystemService.getThirdSystemByType(type);
    }

    /**
     * query thirdSystem by like name.
     */
    @GetMapping(value = "/nameLike/{name}/systemType/{type}", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "query thirdSystem by like name", response = ThirdSystem.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<List<ThirdSystemVo>> getThirdSystemByLikeNmae(
        @ApiParam(value = "name") @PathVariable("name") String name,
        @ApiParam(value = "type") @PathVariable("type") String type) {
        return thirdSystemService.selectByNameLike(name, type);
    }

    /**
     * update a thirdSystem.
     */
    @PutMapping(value = "/{thirdSystemId}", produces = MediaType.APPLICATION_JSON,
        consumes = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "update a thirdSystem", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<String> updateThirdSystem(
        @ApiParam(value = "thirdSystemId") @PathVariable("thirdSystemId") String thirdSystemId,
        @RequestBody ThirdSystem thirdSystem) {
        return thirdSystemService.updateThirdSystem(thirdSystemId, thirdSystem);
    }

    /**
     * delete a thirdSystem.
     */
    @DeleteMapping(value = "/{thirdSystemId}", produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "delete a thirdSystem", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 500, message = "resource grant error", response = String.class)
    })
    @PreAuthorize("hasRole('APPSTORE_TENANT') || hasRole('APPSTORE_ADMIN')")
    public ResponseEntity<String> deleteThirdSystem(
        @ApiParam(value = "thirdSystemId") @PathVariable("thirdSystemId") String thirdSystemId) {
        return thirdSystemService.deleteThirdSystem(thirdSystemId);
    }
}
